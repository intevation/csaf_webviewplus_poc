// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2022 German Federal Office for Information Security (BSI) <https://www.bsi.bund.de>
// Software-Engineering: 2022 Intevation GmbH <https://intevation.de>

import { describe, it, expect } from "vitest";

describe("sum test", () => {
	it("adds 1 + 2 to equal 3", () => {
		expect(1 + 2).toBe(3);
	});
});
