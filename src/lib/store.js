// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2022 German Federal Office for Information Security (BSI) <https://www.bsi.bund.de>
// Software-Engineering: 2022 Intevation GmbH <https://intevation.de>

import { writable } from "svelte/store";

function createStore() {
	const appDefault = {
		data: null
	};
	const { subscribe, set, update } = writable(appDefault);

	return {
		subscribe,
		setData: (/** @type {any} */ data) =>
			update((settings) => {
				settings.data = data;
				return settings;
			}),
		reset: () => set(appDefault)
	};
}

export const appStore = createStore();
