// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 German Federal Office for Information Security (BSI) <https://www.bsi.bund.de>
// Software-Engineering: 2022 Intevation GmbH <https://intevation.de>

/** @type {import('@playwright/test').PlaywrightTestConfig} */
const config = {
	webServer: {
		command: "npm run build && npm run preview",
		port: 4173
	},
	testDir: "tests",
	testMatch: /(.+\.)?(test|spec)\.[jt]s/
};

export default config;
