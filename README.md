<!--
SPDX-License-Identifier: MIT
SPDX-FileCopyrightText: 2022 German Federal Office for Information Security (BSI) <https://www.bsi.bund.de>
Software-Engineering: 2022 Intevation GmbH <https://intevation.de>
-->
# Webview+

This repository is for evaluation purposes only.

![](docs/app.png)

## Usage

### Clone the repo

```git clone https://heptapod.host/intevation/csaf_webviewplus_poc.git```

### `cd` into app directory

```cd csaf_webviewplus_poc```

### Install dependencies

```npm install```

### Run development server and `--open` a browser.

```npm run dev -- --open```

### Drag `CSAF.json` (provided in `exampledata`) over the `dropzone`.
